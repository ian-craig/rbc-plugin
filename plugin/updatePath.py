# Copyright (C) 2014 Ian Craig
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from ttk import Frame
import Tkinter as tk
import tkFileDialog
import os

class SettingsDialogue(Frame):
    path = '/'
  
    def __init__(self, parent):
        Frame.__init__(self, parent)   
        self.parent = parent
        
    def initUI(self):
        self.saved = False
        self.parent.title("Redback CAD - Set Path")

        tk.Label(text="Enter the path at which your CAD files will be stored.",font=('Helvetica', '11')).place(x=20, y=10)

        self.box = tk.Entry(width=50,font=('Helvetica', '11'))
        self.box.place(x=20,y=50)
        self.box.insert(0, self.path)
        
        browse = tk.Button(text='Browse',command=self.browse)
        browse.place(x=430, y=50)
        
        save = tk.Button(text='Save Preferences',command=self.save)
        save.place(x=380, y=90)
        
    def browse(self):
        newpath = tkFileDialog.askdirectory(initialdir=self.path)
        if newpath:
            self.path = newpath
            self.box.delete(0, len(self.box.get()))
            self.box.insert(0, self.path)
        
    def save(self):
        global root
        self.saved = True
        root.destroy()

def updatePath(path):
    # Run window
    global root
    root = tk.Tk()
    root.geometry("500x135+300+300")
    root.resizable(width=False, height=False)
    cwd = os.path.dirname(os.path.realpath(__file__))
    if os.path.isfile(cwd):
        cwd = os.path.dirname(cwd)
    root.wm_iconbitmap(os.path.join(cwd, 'RBCLogo.ico'))
    app = SettingsDialogue(root)
    app.path = path # Give it the current path as default
    app.initUI()
    root.attributes('-topmost', True) # Always on top
    root.mainloop()
    # Deal with result
    if app.saved:
        return app.path
    else:
        return path