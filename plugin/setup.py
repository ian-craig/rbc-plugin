# Copyright (C) 2014 Ian Craig
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from distutils.core import setup
import py2exe

setup(
	name = 'RBC Plugin',
	description = 'RBC Plugin',
	version = '7.0',
#	console=[{'script': 'RBCPlugin.py', 'icon_resources': [(1, 'RBCLogo.ico')]}],
	windows=[{'script': 'RBCPlugin.py', 'icon_resources': [(1, 'RBCLogo.ico')]}],
#	options = {'py2exe': {'bundle_files': 1, 'compressed': True, "dll_excludes": ["tcl85.dll", "tk85.dll"]}},
        options = {"py2exe": {'dll_excludes': ["mswsock.dll", "powrprof.dll"]}},
	zipfile = None,
)