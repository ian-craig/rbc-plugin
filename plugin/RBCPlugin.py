# Copyright (C) 2014 Ian Craig
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
from SocketServer import ThreadingMixIn
from urlparse import urlparse, parse_qs
import sys
import win32com.client

from core import plugin

class RBCHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        self.wfile.write(plugin(urlparse(self.path).path[1:], parse_qs(urlparse(self.path).query)))

    def log_message(self, format, *args):
        return

class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
    """Handle requests in a separate thread."""

def main():
    # Check whether this process was already running
    running = 0
    for objItem in win32com.client.Dispatch("WbemScripting.SWbemLocator").ConnectServer(".","root\cimv2").ExecQuery("Select * from Win32_Process"):
       if sys.argv[0] == objItem.ExecutablePath:
            if running:
                return
            else:
                running += 1

    # Start webserver
    try:
        server = ThreadedHTTPServer(('', 6363), RBCHandler)
        print "################################################################################"
        print "                         Redback CAD Standalone Plugin                          "
        print "                                 Version 7.0                                    "
        print "################################################################################"
        print "Close this window when you are done."
        
        server.serve_forever()
    except KeyboardInterrupt:
        print '^C received, shutting down server'
        server.socket.close()

if __name__ == '__main__':
    main()
