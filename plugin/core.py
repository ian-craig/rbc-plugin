# Copyright (C) 2014 Ian Craig
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import urllib
import urllib2
import os
import sys
import json
import re
import win32api

def plugin(action, opts):
    domain = "http://rbc.redbackracing63.com"
    version = 8
    cwd = os.path.dirname(os.path.realpath(__file__))
    if os.path.isfile(cwd):
        cwd = os.path.dirname(cwd)
    confFile = os.path.join(cwd, "rbc.conf")
    
    # Load config
    computerID, basePath = loadConf(confFile)

    # Update config file
    if action == 'setidentity':
        computerID = opts['id'][0]
        # For now we will also force an update path
        from updatePath import updatePath
        basePath = updatePath(basePath)
        writeConf(confFile, computerID, basePath)
        return "OK"
    
    elif action == 'updatepath':
        from updatePath import updatePath
        basePath = updatePath(basePath)
        writeConf(confFile, computerID, basePath)
        return "OK"

    elif action == 'identity':
#        print '%s/plugin/%s/identity?id=%s&version=%d' % (domain, opts['key'][0], computerID, version)
        try:
            urllib2.urlopen('%s/plugin/%s/identity?id=%s&version=%d' % (domain, opts['key'][0], computerID, version))
        except Exception as e:
            print e
        return computerID

    elif action == 'setup':
        computerID = opts['id'][0]
        from updatePath import updatePath
        basePath = updatePath(basePath)
        writeConf(confFile, computerID, basePath)
        try:
            urllib2.urlopen('%s/plugin/%s/setup?version=%d' % (domain, opts['key'][0], version))
        except urllib2.HTTPError:
            print "Failed to send response to RBC server"
            print '%s/plugin/%s/setup?version=%d' % (domain, opts['key'][0], version)
        return "OK"

    elif not basePath:
        return

    elif action == 'open':
        path = os.path.join(basePath, opts['year'][0])
        
        for filename in opts['file']:
            #Check file extension before we run it!!!
            allowed_ext = ['SLDPRT', 'SLDASM', 'SLDDRW', 'CATPART', 'CATPRODUCT']
            assert os.path.splitext(filename)[1][1:].upper() in allowed_ext
            
            filepath = os.path.join(path, filename)
            #Check file exists
            if os.path.isfile(filepath):
                os.startfile(filepath)
            else:
                print "%s does not exist." % filepath

    elif action == 'update' or action == 'download':
        #Get file list
        try:
            response = urllib2.urlopen('%s/plugin/%s/get_list' % (domain, opts['key'][0]))
            html = response.read()
            updates = json.loads(html)
            #Check folder exists
            assert os.path.isdir(basePath)
        except Exception as e:
            print e

        #Download files
        success = ''
        ps = ''
        for year, files in updates.items():
            if not os.path.isdir(os.path.join(basePath, year)):
                os.makedirs(os.path.join(basePath, year))
            for filename in files:
                success = ''
                #Check file is writable
                try:
                    f = open(os.path.join(basePath, year, filename), 'wb')
                    try:
                        try:
                            response = urllib2.urlopen('%s/plugin/%s/download/%s/%s%s' % (domain, opts['key'][0], year, filename, ps), timeout=5)
                            f.write(response.read())
                            response.close()
                        except socket.timeout:
                            # Timeout. Usually this is a stupid error. Try again with a long timeout
                            print "Error downloading %s %s. Trying again..." % (year, filename)
                            response = urllib2.urlopen('%s/plugin/%s/download/%s/%s%s' % (domain, opts['key'][0], year, filename, ps), timeout=110)
                            f.write(response.read())
                            response.close()
                        # Mark as successfully downloaded
                        success = "?success=%s|%s" % (year, filename)
                    except:
                        print "Error downloading %s %s" % (year, filename)
                        # Let the server know the previous failed
                    f.close()
                except:
                    print "Skipped %s %s" % (year, filename)
                    try:
                        urllib2.urlopen('%s/plugin/%s/skip/%s/%s%s' % (domain, opts['key'][0], year, filename, ps))
                    except:
                        print "Error loading %s/plugin/%s/skip/%s/%s%s" % (domain, opts['key'][0], year, filename, ps)
                # Copy success to previous success so we can notify server on next call
                ps = success
        
        #Tell the server we are done
        urllib2.urlopen('%s/plugin/%s/update%s' % (domain, opts['key'][0], ps))

    elif action == 'upload':
        #Check the file extension is in an allowed set - uploading files on a url request could have security implications.
        filepath = os.path.join(basePath, opts['year'][0], opts['file'][0])
        if not re.match('^.*\\\\201\d\\\\[0-9\-]+\.(SLDPRT|SLDASM)$', filepath, re.IGNORECASE):
            print "I'm not supposed to send that file...\n%s" % filepath
            sys.exit(10)
        
#        print '%s Request: %s/plugin/%s/upload' % (opts['file'][0], domain, opts['key'][0])
        try:
            data = urllib.urlencode({'file': open(filepath, 'rb').read()})    
        except IOError:
            print "File %s not found." % opts['file'][0]
            data = urllib.urlencode({'file_not_found': 1})
        try:
            response = urllib2.urlopen('%s/plugin/%s/upload' % (domain, opts['key'][0]), data)
            html = response.read()
            print '%s Response: %s' % (opts['file'][0], html)
        except Exception as e:
            print e
#        print "\n"

    elif action == 'listopen':
        files = []
        for f in os.listdir(basePath):
            if not re.match('[0-9]{4}(-[0-9]{4})?\.(SLDPRT|SLDASM)$', f, re.IGNORECASE):
                continue
            try:
                open(f, 'r+')
            except IOError:
                files.append(f[:4])
        data = urllib.urlencode({'files': json.dumps(files)})
        urllib2.urlopen('%s/plugin/%s/list_open' % (domain, opts['key'][0]), data)

    return ""

def loadConf(confFile):
    computerID = 0
    basePath = ""
    fileExists = False
    try:
        f = open(confFile, 'r')
        fileExists = True
        computerID = re.match("ID: (\d+)", f.readline()).group(1)
        basePath = re.match("PATH: (.+)", f.readline()).group(1)
        f.close()
    except Exception:
        pass

    if not fileExists:
        writeConf(confFile, computerID, basePath)
    return (computerID, basePath)

def writeConf(confFile, computerID, basePath):
    # Make it visible
    try:
        win32api.SetFileAttributes(confFile, 0) # 0 is normal mode
    except:
        pass
    # Write
    try:
        f = open(confFile, 'w')
        f.write("ID: %s\nPATH: %s\n" % (computerID, basePath))
    except Exception as e:
        print e
    finally:
        f.close()
    # Make it hidden
    try:
        win32api.SetFileAttributes(confFile, 2) # 2 is hidden mode
    except:
        pass